<?php
/** Functional tests for the IMT2571 Assignment #1 application.
 *
 * The tests are using the Behat Mink for interfacing the web server.
 * @author Rune Hjelsvold
 * @see http://mink.behat.org/en/latest/
 */

namespace NO\NTNU\IMT2571\Assignment1\Controller;

require_once('../src/Model/Book.php');

use NO\NTNU\IMT2571\Assignment1\Model\Book;
use PHPUnit\Framework\TestCase;
use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Element\NodeElement;

require_once("vendor/autoload.php");
// Please set the proper test setup values for your system in TestProps.php
require_once("TestProps.php");

/**
 * Class for testing the functionality of an Assignment #1 implementation
 * in IMT2571.
 * @author Rune Hjelsvold
 */
class FunctionalTests extends TestCase
{
    /**
     * Index of the collection page title in the $PAGE_TITLES array
     * @see $PAGE_TITLES 
     */
    const COLLECTION_PAGE_TITLE_IDX = 0;

    /**
     * Index of the details page title in the $PAGE_TITLES array
     * @see $PAGE_TITLES 
     */
    const DETAILS_PAGE_TITLE_IDX = 1;

    /**
     * Index of the error page title in the $PAGE_TITLES array
     * @see $PAGE_TITLES 
     */
    const ERROR_PAGE_TITLE_IDX = 2;

    /**
     * Constant array for holding page titles
     * @see $PAGE_TITLES 
     * @static
     */
    protected static $PAGE_TITLES = array(
                        'Book Collection',
                        'Book Details',
                        'Error Page'
    );
    
    /**
     * Holds the root URL for the Oblig 1 site.
     */
    protected $baseUrl = TEST_BASE_URL;
    
    /**
     * The Mink Session object.
     * @see 
     */
    protected $session;
    /**
     * Holds the id of books created during testing for cleanup during teardown.
     */
    protected $testBookId = null;

    /**
     * Initiates the testing session
     * @see teardown
     */
    protected function setup()
    {
        // Create cleanup array
        $driver = new \Behat\Mink\Driver\GoutteDriver();
        $this->session = new \Behat\Mink\Session($driver);
        $this->session->start();
    }
        
    /**
     * Cleans up system at the end of a test.
     */
    protected function teardown()
    {
        // Remove book entry if added during test
        if ($this->testBookId) {
            $this->removeBookEntry($this->testBookId);
        }
        $this->testBookId = null;
    }
    
    
    /**
     * Computes the number of books in the listed collection.
     * @param DocumentElement $page the web page containing the book list, or the site root page
     *        if no page reference is passed.
     * @return integer the number of books listed on the page.
     */
    protected function getBookListLength($page = null)
    {
        if (!$page) {
            $this->session->visit($this->baseUrl);
            $page = $this->session->getPage();
        }
        return sizeof($page->findAll('xpath', '//table[@id="bookList"]/tbody/tr'));
    }
        
    /**
     * Checks if the page is the expected one - based on page title.
     * @param DocumentElement $page The root element of page to be checked.
     * @param integer $expectedIdx The index of the expected page - within the
     *                $PAGE_TITLE array
     * @param string $bookAuthor book author
     * @static
     * @see $PAGE_TITLES
     */
    protected static function isExpectedPage($page, $expectedIdx)
    {
        $title = null;
        $titleEl = $page->find('xpath', 'head/title');
        
        // Page has a title element
        if ($titleEl) {
            $title = $titleEl->getText();
        }
        
        // Compare title to the expected value
        return $title === self::$PAGE_TITLES[$expectedIdx];
    }
    
    /**
     * Adds a book to the collection. The id of the new created book is stored
     * in $testBookId for cleanup purposes.
     * @param \NO\NTNU\IMT2571\Assignment1\Model\Book $book Book object to be added to the
     *        collection. id will be set when the book is added to the collection.
     * @see teardown()
     */
    protected function addBook($book)
    {
        // Load book list to get to the addForm
        $this->session->visit($this->baseUrl);
        $page = $this->session->getPage();
        $addForm = $page->find('xpath', '//form[@id="addForm"]');
        
        // Complete and submit addForm
        $addForm->find('xpath', 'input[@name="title"]')->setValue($book->title);
        $addForm->find('xpath', 'input[@name="author"]')->setValue($book->author);
        $addForm->find('xpath', 'input[@name="description"]')->setValue($book->description);
        $addForm->submit();
        
        $page = $this->session->getPage();

        $newBook = $page->find('xpath', '//*[@id="newBook"]');
        if ($newBook) {
            // Retrieve the id from the book list page
            $book->id = $newBook->find('xpath', './/*[@id="newBookId"]')->getText();
            $this->testBookId = $book->id;
        }
    }
        
    /**
     * Deletes a book from the collection. The id of the newly created book is removed
     * from $testBookId because it no longer needs to be cleaned up.
     * @param integer $id Id of the book to be deleted from the collection.
     * @see teardown()
     * @uses removeBookEntry()
     */
    protected function deleteBook($id)
    {
        // Remove book entry in collection
        $this->removeBookEntry($id);
        $this->testBookId = null;
    }
    
    /**
     * Removes a book entry from the web site collection
     * @param integer $bookId id of the book to be removed from the collection.
     * @see teardown()
     */
    protected function removeBookEntry($bookId)
    {
        // Load page containing form to delete the book entry
        $this->session->visit($this->baseUrl . '?id=' . $bookId);
        $page = $this->session->getPage();
        
        // Submit the delete form
        $delForm = $page->find('xpath', '//form[@id="delForm"]');
        $delForm->submit();
    }
    
    /**
     * Modifies data about a book in the collection
     * @param Book $book Book object to be modified.
     */
    protected function modifyBook($book)
    {
        // Load page containing form to modify the book entry
        $this->session->visit($this->baseUrl . '?id=' . $book->id);
        $page = $this->session->getPage();

        // Complete and submit the form to modify the book entry
        $modForm = $page->find('xpath', '//form[@id="modForm"]');
        $modForm->find('xpath', 'input[@name="title"]')->setValue($book->title);
        $modForm->find('xpath', 'input[@name="author"]')->setValue($book->author);
        $modForm->find('xpath', 'input[@name="description"]')->setValue($book->description);
        $modForm->submit();
    }

    /**
     * Asserts that data about a book in the collection matches the passed book.
     * Also asserts that the data matches the data on the book details page.
     * @param Book $book The book object to be verified.
     * @uses assertBookDetails()
     */
    protected function assertBookListEntry($book)
    {
        // Load book list to get to the book entries
        $this->session->visit($this->baseUrl);
        $page = $this->session->getPage();

        // Find the book entry and verify the data matches the expected value
        $bookNode = $page->find('xpath', '//table[@id="bookList"]/tbody/tr[@id="book'
                                         . $book->id . '"]');
        if ($book) {
            $this->assertEquals($book->id, $bookNode->find('xpath', 'td[1]/a')->getText(), 'Unexpected id');
            $detailsLink = $bookNode->find('xpath', 'td[1]/a');
            $this->assertEquals($book->title, $bookNode->find('xpath', 'td[2]')->getText(), 'Unexpected title');
            $this->assertEquals($book->author, $bookNode->find('xpath', 'td[3]')->getText(), 'Unexpected author');
            $this->assertEquals($book->description, $bookNode->find('xpath', 'td[4]')->getText(), 'Unexpected description');
        
            // Further verify that the content is the same on the details page
            $this->assertBookDetails($detailsLink, $book);
        } else {
            $this->assertFalse(true, "Expecting book record to exist in book list");
        }
    }

    /**
     * Asserts that data about a book matches the data displayed on the book details page.
     * @param NodeElement $detailsLink The hyperlink element for the details page.
     * @param Book $book The book object to be verified.
     */
    protected function assertBookDetails($detailsLink, $book)
    {
        // Load book details page
        $detailsLink->click();
        $page = $this->session->getPage();

        // Verify values shown on form
        $modForm = $page->find('xpath', '//form[@id="modForm"]');
        $this->assertEquals($book->id, $modForm->find('xpath', 'input[@name="id"]')->getValue(), 'Unexpected book id');
        $this->assertEquals(
            $book->title,
            $modForm->find('xpath', 'input[@name="title"]')->getValue(),
            'Unexpected book title'
        );
        $this->assertEquals(
            $book->author,
            $modForm->find('xpath', 'input[@name="author"]')->getValue(),
            'Unexpected book author'
        );
        $this->assertEquals(
            $book->description,
            $modForm->find('xpath', 'input[@name="description"]')->getValue(),
            'Unexpected book description'
        );
    }
    
    /**
     * Attempts to add a book - asserting it to be successful
     * @param Book $book The book object to be added.
     */
    protected function attemptSuccessfulAddBook($book)
    {
        $bookListLength = $this->getBookListLength();
        $this->addBook($book);

        // Verifying book content in book list and on book details page
        $this->assertEquals(
            $bookListLength + 1,
            $this->getBookListLength(),
            'Expecting that a new book was added to collection'
        );
        $this->assertBookListEntry($book);
    }

    /**
     * Attempts to add a book - asserting it to be unsuccessful
     * @param Book $book The book object to be added.
     */
    protected function attemptUnsuccessfulAddBook($book)
    {
        $this->addBook($book);

        // Verifying that error page is returned
        $page = $this->session->getPage();
        $this->assertTrue(
            self::isExpectedPage($page, self::ERROR_PAGE_TITLE_IDX),
            'Expecting error page'
        );
    }
    
    /**
     * Attempts to modify a book - asserting it to be unsuccessful
     * @param Book $book The book object to be modified.
    */
    public function attemptSuccessfulModifyBook($book)
    {
        $oldBook = new Book('To be modified', 'To be modified', 'To be modified');
        $this->addBook($oldBook);
        
        $book->id = $oldBook->id;
        $this->modifyBook($book);
        
        // Verifying that book information has been changed
        $this->assertBookListEntry($book);
    }
    
    /**
     * Attempts to modify a book - asserting it to be unsuccessful
     * @param Book $book The book object to be modified.
     */
    public function attemptUnsuccessfulModifyBook($book)
    {
        $oldBook = new Book('To be modified', 'To be modified', 'To be modified');
        $this->addBook($oldBook);
        
        $book->id = $oldBook->id;
        $this->modifyBook($book);
        
        // Verifying that book information is left unchanged
        $this->assertBookListEntry($oldBook);
    }
    
    /**
     * Test that the structure of book collection page is as expected.
     */
    public function testBookCollectionPage()
    {
        $this->session->visit($this->baseUrl);
        $page = $this->session->getPage();

        // Verifying that a collection page was returned
        $this->assertTrue(
            self::isExpectedPage($page, self::COLLECTION_PAGE_TITLE_IDX),
            'Expecting collection page'
        );

        // Verifying the presence of the form for adding new books
        $addForm = $page->find('xpath', '//form[@id="addForm"]');
        $this->assertNotNull($addForm, 'Expecting addForm to be present');

        // Verifying the presence of the operator for adding new books
        $addOp = $addForm->find('xpath', 'input[@name="op"]');
        $this->assertEquals('add', $addOp->getValue(), 'Expecting add operation input field');
    }
    
    /**
     * Test that the structure of book details page is as expected
     * @depends testBookCollectionPage
     */
    public function testBookDetailsPage()
    {
        $book = new Book('Test title', 'Test author', 'Test author');
        $this->addBook($book);

        // Load book details page
        $this->session->visit($this->baseUrl . '?id=' . $book->id);
        $page = $this->session->getPage();

        // Verifying page title
        $this->assertTrue(
            self::isExpectedPage($page, self::DETAILS_PAGE_TITLE_IDX),
            'Expecting details page'
        );

        // Verifying the form for modifying book data
        $form = $page->find('xpath', '//form[@id="modForm"]');
        $this->assertNotNull($form, 'Expecting modForm to be present');
        $op = $form->find('xpath', 'input[@name="op"]');
        $this->assertEquals('mod', $op->getValue(), 'Expecting mod operation to be present');

        // Verifying the form for deleting book entries
        $form = $page->find('xpath', '//form[@id="delForm"]');
        $this->assertNotNull($form, 'Expecting delForm to be present');
        $op = $form->find('xpath', 'input[@name="op"]');
        $this->assertEquals('del', $op->getValue(), 'Expecting del operation to be present');
    }
    
    /**
     * Tests that adding a book with ordinary field contents succeeds.
     * @depends testBookDetailsPage
     */
    public function testAddOrdinary()
    {
        $book = new Book('Test title', 'Test author', 'Test description');
        $this->attemptSuccessfulAddBook($book);
    }
    
    /**
     * Tests that adding a book where fields contain single quotes succeeds.
     * @depends testBookDetailsPage
     */
    public function testAddContainsSingleQuote()
    {
        $book = new Book("Test 'title'", "Test 'author'", "Test 'description'");
        $this->attemptSuccessfulAddBook($book);
    }
    
    /**
     * Tests that adding a book where fields contain HTML special characters succeeds.
     * @depends testBookDetailsPage
     */
    public function testAddContainsSpecialHtmlChars()
    {
        $book = new Book(
            '<script document.body.style.visibility="hidden" />',
            '<script document.body.style.visibility="hidden" />',
            '<script document.body.style.visibility="hidden" />'
        );
        $this->attemptSuccessfulAddBook($book);
    }
    
    /**
     * Tests that adding a book with an empty title field fails.
     * @depends testBookDetailsPage
     */
    public function testAddEmptyTitle()
    {
        $book = new Book('', 'Test author', 'Test description');
        $this->attemptUnsuccessfulAddBook($book);
    }
    
    /**
     * Tests that adding a book with an empty author field fails.
     * @depends testBookDetailsPage
     */
    public function testAddEmptyAuthor()
    {
        $book = new Book('Test title', '', 'Test description');
        $this->attemptUnsuccessfulAddBook($book);
    }
    
    /**
     * Tests that modifying a book succeeds for ordinary field contents.
     * @depends testBookDetailsPage
     */
    public function testModifyOrdinary()
    {
        $book = new Book('New title', 'New author', 'New description');
        $this->attemptSuccessfulAddBook($book);
    }
    
    /**
     * Tests that modifying a book succeeds when fields contain single quotes.
     * @depends testBookDetailsPage
     */
    public function testModifyContainsSingleQuote()
    {
        $book = new Book("Test 'title'", "Test 'author'", "Test 'description'");
        $this->attemptSuccessfulAddBook($book);
    }
    
    /**
     * Tests that modifying a book succeeds when fields contain HTML special characters.
     * @depends testBookDetailsPage
     */
    public function testModifyContainsSpecialHtmlChars()
    {
        $book = new Book(
            '<script document.body.style.visibility="hidden" />',
            '<script document.body.style.visibility="hidden" />',
            '<script document.body.style.visibility="hidden" />'
        );
        $this->attemptSuccessfulAddBook($book);
    }
    
    /**
     * Tests that modifying a book fails when title is empty.
     * @depends testBookDetailsPage
     */
    public function testModifyEmptyTitle()
    {
        $book = new Book('', 'Test author', 'Test description');
        $this->attemptUnsuccessfulModifyBook($book);
    }
    
    /**
     * Tests that modifying a book fails when author is empty.
     * @depends testBookDetailsPage
     */
    public function testModifyEmptyAuthor()
    {
        $book = new Book('Test title', '', 'Test description');
        $this->attemptUnsuccessfulModifyBook($book);
    }
    
    /**
     * Tests deleting a book record from the collection.
     * @depends testBookDetailsPage
     */
    public function testBookDelete()
    {
        $book = new Book('To be deleted', 'To be deleted', 'To be deleted');
        $this->addBook($book);
        $bookListLength = $this->getBookListLength();
        
        $this->deleteBook($book->id);
        $this->session->visit($this->baseUrl);
        $page = $this->session->getPage();

        // Verifying that book is removed from book list
        $this->assertEquals(
            $bookListLength-1,
            $this->getBookListLength($page),
            'Expecting book list to shrink in length'
        );
        $bookNode = $page->find('xpath', '//table[@id="bookList"]/tbody/tr[@id="book' . $book->id . '"]');
        $this->assertNull($bookNode, 'Expecting book to be removed from book list');
    }
    
    /**
     * Tests if book details page is open for SQL injection - i.e., if non-numeric
     * characters are accepted in id parameter
     * @depends testBookDetailsPage
     */
    public function testSqlInjection()
    {
        $book = new Book('New title', 'New author', 'New description');
        $this->addBook($book);

        $this->session->visit($this->baseUrl . '?id=' . $book->id . "'; drop table books;--");
        $page = $this->session->getPage();

        // Verifying that request was rejected
        $this->assertTrue(
            self::isExpectedPage($page, self::ERROR_PAGE_TITLE_IDX),
            'Expecting error page'
        );
    }
}

