<?php
/** Unit tests for DBModel in the IMT2571 Assingment #1 application.
 *
 * The tests are making use of the DbUnit testing framework in PHPUnit.
 * @author Rune Hjelsvold
 * @see https://phpunit.de/manual/current/en/database.html
 */

namespace NO\NTNU\IMT2571\Assignment1\Model;

require_once("vendor/autoload.php");
require_once(__DIR__ . "/../src/Model/DBModel.php");
require_once("TestProps.php");

use Exception;
use PDO;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
// Included because of the documentation
use PHPUnit_Extensions_Database_DataSet_IDataSet;
use PHPUnit_Extensions_Database_DB_IDatabaseConnection;

/**
 * Unit test for DBModel class.
 * @see DBModel
 */
class DBModelTest extends TestCase
{
    use TestCaseTrait;

    /**
      * only instantiate pdo once for test clean-up/fixture load
      * @var PDO
      * @static
      */
    protected static $pdo = null;

    /**
      * only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
      * @var PHPUnit_Extensions_Database_DB_IDatabaseConnection
      * @static
      */
    private $conn = null;
    
    /**
     * Constant for an invalid id - can be used to test operations called on non-existing books
     */
    const INVALID_BOOK_ID = -99;
    
    /**
     * Returns an array of test case books. The array should always be in synch with the
     * XML fixture file.
     * @return Book[] Array of test case books.
     * @static
     */
    protected static function getTestBooks()
    {
        return array(new Book('Jungle Book', 'R. Kipling', 'A classic book.', 1),
                     new Book('Moonwalker', 'J. Walker', null, 2),
                     new Book(
                         'PHP & MySQL for Dummies',
                         'J. Valade',
                              'Written by some smart gal.',
                         3
                     ));
    }
    
    /**
     * Returning the PDO object for the database to be used for tests.
     * @return PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO(
                    'mysql:dbname=' . TEST_DB_NAME . ';host=' . TEST_DB_HOST,
                    TEST_DB_USER, TEST_DB_PWD,
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
                );
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, TEST_DB_NAME);
        }

        return $this->conn;
    }
    
    /**
     * Returning the fixtures.
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet()
    {
        return $this->createXMLDataSet(FIXTURE_XML_FILE);
    }

    /**
     * Attempts to add a book - asserting it to be successful
     * @param Book $book The book object to be added.
     */
    protected function attemptSuccessfulAdd($book)
    {
        $model = new DBModel(self::$pdo);
        $dbSize = $this->getConnection()->getRowCount(TEST_TABLE_NAME);
        
        $model->addBook($book);
                
        // Was the id set?
        $this->assertGreaterThan(
            0,
            $book->id,
            'Expected book id to be set in DBModel::addBook()'
        );
                
        // Was the book added?
        $this->assertEquals(
            $dbSize + 1,
            $this->getConnection()->getRowCount(TEST_TABLE_NAME),
            'Expecting that a new row was added to the database'
        );
        $this->assertBookData($book, $model->getBookById($book->id));
    }
    
    /**
     * Attempts to add a book - asserting it to be unsuccessful
     * @param Book $book The book object to be added.
     */
    protected function attemptUnsuccessfulAdd($book)
    {
        $model = new DBModel(self::$pdo);
        $dbSize = $this->getConnection()->getRowCount(TEST_TABLE_NAME);

        try {
            $model->addBook($book);
            $this->assertFalse(true, 'Expected DBModel::addBook() to fail on invalid data');
        } catch (Exception $e) {
            // Added to keep dbUnit from classifying this as a risky test
            $this->assertTrue(true, 'Expected DBModel::addBook() to fail on invalid data');
        }
    }
    
    /**
     * Attempts to modify a book - asserting it to be successful. The book's id
     * will be replaced with the id of the first book in the test collection if set to -1.
     * @param Book $book The book object to be modified.
    */
    public function attemptSuccessfulModify($book)
    {
        if ($book->id == -1) {
            // Use the first book in the test database as the target of the change
            $book->id = $this->getTestBooks()[0]->id;
        }
        $model = new DBModel(self::$pdo);
        $model->modifyBook($book);

        // Verify that data was correctly changed
        $modifiedBook = $model->getBookById($book->id);
        $this->assertBookData($book, $modifiedBook);
    }
    
    /**
     * Attempts to modify a book - asserting it to be unsuccessful. The book's id
     * may be unset to have the non-id values being checked or may be set to have
     * updates for invalid id's being attempted.
     * @param Book $book The book object to be modified. If $book->id is set,
     *                   the id value will be checked; otherwise, it will be set
     *                   to the id of the first book in the test collection.
    */
    public function attemptUnsuccessfulModify($book)
    {
        $oldBook = null;
        if ($book->id == -1) {
            // Use the first book in the test database as the target of the change
            $oldBook = $this->getTestBooks()[0];
            $book->id = $oldBook->id;
        }
        try {
            $model = new DBModel(self::$pdo);
            $model->modifyBook($book);
            $this->assertFalse(true, 'Expected DBModel::modifyBook() to fail on invalid data');
        } catch (Exception $e) {
            // Added to keep dbUnit from classifying this as a risky test
            $this->assertTrue(true, 'Expected DBModel::modifyBook() to fail on invalid data');

            // Check to see that nothing was updated
            if ($oldBook) {
                $book = $model->getBookById($oldBook->id);
                $this->assertBookData($oldBook, $book);
            }
        }
    }
    
    /**
     * Tests that the complete book list is retrieved
     */
    public function testGetBookList()
    {
        $testBooks = self::getTestBooks();
        $model = new DBModel(self::$pdo);
        $bookList = $model->getBookList();
        
        // Are all books from the fixtures loaded?
        $this->assertEquals(
            sizeof($testBooks),
            sizeof($bookList),
                            "Unexpected booklist length"
        );
        
        // Is each book correctly represented in database?
        for ($cnt = 0; $cnt < sizeof($testBooks); $cnt++) {
            $this->assertBookData($testBooks[$cnt], $bookList[$cnt]);
        }
    }
    
    /**
     * Tests that individual book records can be retrieved
     */
    public function testGetBook()
    {
        $testBooks = self::getTestBooks();
        $model = new DBModel(self::$pdo);
        
        // Can all books from the fixtures be recalled?
        foreach ($testBooks as $testBook) {
            $book = $model->getBookById($testBook->id);
            $this->assertNotNull($book, 'Expected to get book with id=' . $testBook->id);
            $this->assertBookData($testBook, $book);
        }
        
        // No book should be returned for invalid ids
        $this->assertNull($model->getBookById(self::INVALID_BOOK_ID), "No book expected for invalid id.");
    }
    
    /**
     * Tests that adding a book with ordinary field contents succeeds.
     * @depends testGetBook
     */
    public function testAddOrdinary()
    {
        $book = new Book('Test title', 'Test author', 'Test description');
        $this->attemptSuccessfulAdd($book);
    }
    
    /**
     * Tests that adding a book where fields contain single quotes succeeds.
     * @depends testAddOrdinary
     */
    public function testAddContainsSingleQuote()
    {
        $book = new Book("Test 'title'", "Test 'author'", "Test 'description'");
        $this->attemptSuccessfulAdd($book);
    }
    
    /**
     * Tests that adding a book with an empty title field fails.
     * @depends testAddOrdinary
     */
    public function testAddEmptyTitle()
    {
        $book = new Book('', 'Test author', 'Test description');
        $this->attemptUnsuccessfulAdd($book);
    }
    
    /**
     * Tests that adding a book with an empty author field fails.
     * @depends testAddOrdinary
     */
    public function testAddEmptyAuthor()
    {
        $book = new Book('Test title', '', 'Test description');
        $this->attemptUnsuccessfulAdd($book);
    }
    
    /**
     * Tests that modifying a book succeeds for ordinary field contents.
     * @depends testGetBook
     */
    public function testModifyOrdinary()
    {
        $book = new Book('New title', 'New author', 'New description');
        $this->attemptSuccessfulModify($book);
    }
    
    /**
     * Tests that modifying a book succeeds when fields contain single quotes.
     * @depends testModifyOrdinary
     */
    public function testModifyContainsSingleQuote()
    {
        $book = new Book("Test 'title'", "Test 'author'", "Test 'description'");
        $this->attemptSuccessfulModify($book);
    }
    
    /**
     * Tests that modifying a book fails when id's contain non-numeric characters.
     * @depends testModifyOrdinary
     */
    public function testModifyInvalidId()
    {
        $book = new Book('Test', 'Test', 'Test', "4';drop table book;--");
        $this->attemptUnsuccessfulModify($book);
    }
    
    /**
     * Tests that modifying a book to an empty title field fails.
     * @depends testAddOrdinary
     */
    public function testModifyEmptyTitle()
    {
        $book = new Book('', 'Test author', 'Test description');
        $this->attemptUnsuccessfulModify($book);
    }
    
    /**
     * Tests that modifying a book to an empty author field fails.
     * @depends testAddOrdinary
     */
    public function testModifyEmptyAuthor()
    {
        $book = new Book('Test title', '', 'Test description');
        $this->attemptUnsuccessfulModify($book);
    }

    /**
     * Tests that deletion succeeds for valid id's
     */
    public function testDeleteValidId()
    {
        $model = new DBModel(self::$pdo);
        $dbSize = $this->getConnection()->getRowCount(TEST_TABLE_NAME);
        
        // Using the first test case as the target for deletions
        $book = self::getTestBooks()[0];
        $model->deleteBook($book->id);
        
        // Verifying that a book has disappeared
        $this->assertEquals(
            $dbSize - 1,
            $this->getConnection()->getRowCount(TEST_TABLE_NAME),
            "Expecting book table size to decrement"
        );
        $this->assertNull(
            $model->getBookById($book->id),
            "Expecting book to have been removed from collection"
        );
    }
    
    /**
     * Test that deletion fails for invalid id's - in this case to stop an SQL injection attempt
     */
    public function testDeleteSqlInjection()
    {
        $invalidId = "4';drop table book;--";
        $model = new DBModel(self::$pdo);
        
        try {
            $model->deleteBook($invalidId);
            $this->assertFalse(true, 'Expecting DBModel::deleteBook() to fail on invalid id');
        } catch (Exception $e) {
            // Verifying that book table is unchanged
            $origTable = $this->createXMLDataSet(FIXTURE_XML_FILE)
                              ->getTable(TEST_TABLE_NAME);
            $updatedTable = $this->getConnection()->createQueryTable(
                TEST_TABLE_NAME,
                'SELECT * FROM ' . TEST_TABLE_NAME
            );
            $this->assertTablesEqual($origTable, $updatedTable);
        }
    }
    
    /**
     * Asserts that two book records are equal.
     * @param Book $testBook Book object to be compared against
     * @param Book $book Book object to test
     */
    protected function assertBookData($testBook, $book)
    {
        $this->assertEquals($testBook->id, $book->id, "Unexpected book id");
        $this->assertEquals($testBook->title, $book->title, "Unexpected book title");
        $this->assertEquals($testBook->author, $book->author, "Unexpected book author");
        $this->assertEquals($testBook->description, $book->description, "Unexpected book description");
    }
}
